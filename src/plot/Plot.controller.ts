
import { AnyObject } from "../common/AnyObject.interface";
import * as Revalidator from 'revalidator';
import { Query } from "../database/Query";
import { Request, Response } from "express";
import { Database } from "../database/Database";
import * as ShortId from 'shortid';
import { PlotValidator } from "./Plot.validator";
import * as moment from 'moment';
import { WriteResult } from "../../node_modules/@types/rethinkdb";
import { Actor } from "../actor/Actor.controller";
import { Stuff } from "../stuff/Stuff.controller";

export class PlotController {
  public static create(request:Request, response:Response){

    const projectId = request.params.projectId;
    const scenarioId = request.params.scenarioId;
    const plot:Plot = request.body;
    plot.id = ShortId.generate().replace('-', '_');
    plot.creationDate = moment().toISOString();
    plot.scenarioId = scenarioId;

    const validation = new PlotValidator().validate(plot);
    if(validation.valid){
      Database.insert(projectId, 'plots', plot)
      .then(() => response.status(200).json(plot))
      .catch(reason => response.status(400).json(reason))
    } else {
      response.status(400).json(validation.errors);
    }
  }

  public static update(request:Request, response:Response){
    const projectId = request.params.projectId;
    const scenarioId = request.params.scenarioId;
    const id = request.params.id;
    const plot:Plot = request.body;
    const validation = new PlotValidator().validate(plot);
    if(!validation.valid) return response.status(400).json(validation);
    Database.update(projectId, 'plots', { id, scenarioId }, plot)
    .then(() => response.status(200).json(plot))
    .catch(reason => response.status(400).json(reason))
  }

  public static getAll(request:Request, response:Response) {
    const projectId = request.params.projectId;
    const scenarioId = request.params.scenarioId;
    const plots = Database.getTable(projectId, 'plots');
    Database.runOperation(plots.filter({ scenarioId }))
    .then(result => result.toArray().then(value => response.status(200).json(value)))
    .catch(reason => response.status(204).send())
  }

  public static get(request:Request, response:Response){
    const projectId = request.params.projectId;
    const scenarioId = request.params.scenarioId;
    const plotId = request.params.id;
    const plots = Database.getTable(projectId, 'plots');
    Database.runOperation(plots.get(plotId))
    .then(value => response.status(200).json(value))
    .catch(reason => response.status(400).json(reason))
  }
}

export class Plot implements AnyObject{
  
  public id:string;
  public scenarioId:string;
  public name:string;
  public action:string;
  public creationDate?: string;
  public description?:string;
  public result:any;
  public actor:Actor;
  public stuff:Stuff;
  public payload:any;

  public static Actions = {
    VALIDATION: 'validation',
    INSERT: 'insert',
    UPDATE: 'update',
    QUERY: 'query',
    INTEGRATION: 'integration'
  };
}

export class Validation implements AnyObject{
  public schema:any;
  public static validate(instance:Validation, data):ValidationResult{
    let validationResult = new ValidationResult();
    if(data instanceof Array) {
      data.forEach(item => {
        let validation = Revalidator.validate(item, instance.schema);
        if(!validation.valid) validationResult.valid = false;
        validationResult.validations.push(validation);
      });
      return validationResult;
    }
    else {
      let validation = Revalidator.validate(data, instance.schema);
      if(!validation.valid) validationResult.valid = false;
      validationResult.validations.push(validation);
      return validationResult;
    }
  }
}
export class ValidationResult implements AnyObject{
  public valid:boolean = true;
  public validations:Array<Revalidator.IReturnMessage>;
  constructor () { this.validations = new Array(); }
}