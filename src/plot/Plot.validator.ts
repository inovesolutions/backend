import { Plot } from './Plot.controller';
import * as Revalidator from 'revalidator';

export class PlotValidator {
  private plotSchema:Revalidator.JSONSchema<Plot> = {
    properties: {
      id: {
        type: 'string',
        required: true,
        pattern: /^\w+$/
      },
      scenarioId: {
        type: 'string',
        required: true,
        pattern: /^\w+$/
      },
      name: {
        type: 'string',
        required: true,
        allowEmpty: false
      }
    }
  }
  public validate(plot:Plot):Revalidator.IReturnMessage{
    return Revalidator.validate(plot, this.plotSchema);
  }
}