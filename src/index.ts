import app from './App';
import * as morgan from 'morgan';
import { Config } from './Config';
import * as socket from 'socket.io';
import * as cluster from 'cluster';
import * as os from 'os';
import * as redis from 'socket.io-redis';
import * as http from 'http';

if (cluster.isMaster) {
  let server = http.createServer();
  let io = socket.listen(server);
  io.adapter(redis({ host: 'localhost', port: 6379 }))

  for (let i = 0; i < 1; i++) {
    let worker = cluster.fork();
    // worker.send(`Hello worker ${worker.id}, I am your master!`)
    // worker.on('message', (msg) => {
    //   console.log(`Message from worker ${worker.id}: ${msg}`);
    // });
  }

  cluster.on('exist', (worker) => {
    console.log(`worker ${worker.process.pid} died`)
  })
} else {
  let config = new Config();
  const port = process.env.PORT || 3000;
  app.set('secretKey', config.secret);

  let server = app.listen(port, (err) => {
    if (err) return console.log(err);
    return console.log(`${cluster.worker.process.pid} is listening on ${port}`);
  });

  let io = socket.listen(server);
  io.origins('*:*');
  io.adapter(redis({ host: 'localhost', port: 6379 }))
  io.on("connect", (socket: socket.Socket) => {
    console.log(`connect to worker ${cluster.worker.process.pid}`)
  })

  app.set('io', io);
  app.use(morgan('dev'));

  // cluster.worker.on('message', (msg) => {
  //   console.log(`Message from master received by worker ${cluster.worker.id}: ${msg}`);
  // });
  // process.send(`Hello master, I am the worker ${cluster.worker.id}!`);
}
