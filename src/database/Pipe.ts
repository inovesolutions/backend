import { AnyObject } from "../common/AnyObject.interface";

export class Pipe implements AnyObject{
  public table:string;
  public name:string;
  public value:any;
  public type:string;
  public expression:[Pipe];
}