
import * as RethinkDB from 'rethinkdb';
import { Config } from '../Config';

export class Database {

  private static connection: RethinkDB.Connection;
  private static config:Config = new Config();

  public static connect(): Promise < any > {
    return new Promise((resolve, reject) => {
      RethinkDB.connect(this.config.connectionOptions)
        .then(connection => {
          this.connection = connection;
          resolve(connection);
        }).catch(reason => reject(reason))
    });
  }

  public static tableExists(dbName: string, tableName: string): Promise < boolean > {
    return new Promise((resolve, reject) => {
      RethinkDB.db(dbName).tableList().run(this.connection)
        .then(tables => {
          resolve(!!tables.find(table => table === tableName))
        }).catch(reason => reject(reason))
    });
  }

  public static dbExists(dbName: string): Promise < boolean > {
    return new Promise((resolve, reject) => {
      RethinkDB.dbList().run(this.connection)
        .then(databases => {
          resolve(!!databases.find(db => db === dbName))
        }).catch(reason => reject(reason))
    });
  }

  public static insert(dbName: string, tableName: string, data: any):Promise<RethinkDB.WriteResult> {
    return new Promise((resolve, reject) => {
      const insertOperation = RethinkDB.db(dbName).table(tableName).insert(data);
      this.prepare(dbName, tableName)
        .then(() =>
          insertOperation.run(this.connection)
          .then(result => resolve(result))
          .catch(reason => reject(reason)))
        .catch(reason => reject(reason))
    })
  }

  public static update(dbName: string, tableName: string, query: any, data: any):Promise<any> {
    return new Promise((resolve, reject) => {
      const updateOperation = RethinkDB.db(dbName).table(tableName)
        .filter(query).update(data);
      this.prepare(dbName, tableName)
        .then(() =>
          updateOperation.run(this.connection)
            .then(result => resolve(result))
            .catch(reason => reject(reason)))
        .catch(reason => reject(reason))
    });
  }

  public static getTable(dbName:string, tableName:string):RethinkDB.Table{
    return RethinkDB.db(dbName).table(tableName);
  }

  public static runOperation(operation:RethinkDB.Operation<any>):Promise<RethinkDB.Cursor>{
    return operation.run(this.connection);
  }

  public static runUpdate(writeable:RethinkDB.Writeable, data:any):Promise<any>{
    return new Promise((resolve, reject) => {
      writeable.update(data).run(this.connection)
      .then(result => resolve(result))
      .catch(reason => reject(reason))
    })
  }

  private static prepare(dbName: string, tableName: string): Promise < any > {
    return new Promise((resolve, reject) => {
      this.dbExists(dbName).then(exists => {
        if (exists) {
          this.tableExists(dbName, tableName)
            .then(exists => {
              if (!exists) RethinkDB.db(dbName).tableCreate(tableName).run(this.connection)
                .then(result => resolve(result))
                .catch(reason => reject(reason))
              else resolve()
            }).catch(reason => reject(reason))
        } else {
          RethinkDB.dbCreate(dbName).run(this.connection)
            .then(result => {
              RethinkDB.db(dbName).tableCreate(tableName).run(this.connection)
                .then(result => resolve(result))
                .catch(reason => reject(reason))
            }).catch(reason => reject(reason))
        }
      });
    });
  }
}