import * as RethinkDB from 'rethinkdb';
import { AnyObject } from '../common/AnyObject.interface';
import { Pipe } from './Pipe';
import { Cursor } from 'rethinkdb';

export class Query implements AnyObject {
  public tableName:string;
  public pipeline: Array<Pipe>;

  public static from(instance:Query, table: RethinkDB.Table):RethinkDB.Operation<Cursor> {
    instance.pipeline.forEach(pipe => {
      switch (pipe.type) {
        case 'expression':
          let expression = null;
          pipe.expression.forEach(p => {
            if (!expression)  expression = RethinkDB[p.name](p.value); 
            else expression = expression[p.name](p.value);
          })
          table = table[pipe.name](expression);
          break;
        case 'js':
          table = table[pipe.name](RethinkDB.js(pipe.value));
          break;
        case 'all':
          table = table[pipe.name](...pipe.value);
        case 'beetwen':
          table = table[pipe.name](pipe.value.lower, pipe.value.upper, pipe.value.index);
        default:
          if (pipe.value) table = table[pipe.name](pipe.value);
          else table = table[pipe.name]();
      }
    })

    return table;
  }
}