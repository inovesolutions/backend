import * as Revalidator from 'revalidator';
import { Stuff } from './Stuff.controller';

export class StuffValidator {
  private actorSchema:Revalidator.JSONSchema<any> = {
    properties: {
      id: {
        type: 'string',
        required: true
      },
      name: {
        type: 'string',
        required: true
      }
    }
  }

  public validate(stuff:Stuff):Revalidator.IReturnMessage{
    return Revalidator.validate(stuff, this.actorSchema);
  }
}