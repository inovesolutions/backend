import { AnyObject } from "../common/AnyObject.interface";
import { Request, Response } from "express";
import { StuffValidator } from "./Stuff.validator";
import { Database } from "../database/Database";
import * as ShortId from 'shortid';
import * as moment from 'moment';

export class StuffController {
  public static create(request:Request, response:Response){
    const projectId = request.params.projectId;
    const stuff:Stuff = request.body;
    stuff.id = ShortId.generate().replace('-', '_');
    stuff.creationDate = moment().toISOString();
    const validation = new StuffValidator().validate(stuff);
    if(!validation.valid) return response.status(400).json(validation);
    Database.insert(projectId, 'stuffs', stuff)
    .then(value => response.status(200).json(stuff))
    .catch(reason => response.status(400).json(reason))
  }

  public static update(request:Request, response:Response){
    const projectId = request.params.projectId;
    const stuffId = request.params.id;
    const stuff:Stuff = request.body;
    const validation = new StuffValidator().validate(stuff);
    if(!validation.valid) return response.status(400).json(validation);
    Database.update(projectId, 'stuffs', {id: stuffId}, stuff)
    .then(value => response.status(200).json(stuff))
    .catch(reason => response.status(400).json(reason))
  }

  public static getAll(request:Request, response:Response) {
    const projectId = request.params.projectId;
    const stuffs = Database.getTable(projectId, 'stuffs');
    Database.runOperation(stuffs)
    .then(result => result.toArray().then(value => response.status(200).json(value)))
    .catch(reason => response.status(204).send())
  }

  public static get(request:Request, response:Response){
    const projectId = request.params.projectId;
    const stuffId = request.params.id;
    const stuffs = Database.getTable(projectId, 'stuffs');
    Database.runOperation(stuffs.get(stuffId))
    .then(value => response.status(200).json(value))
    .catch(reason => response.status(400).json(reason))
  }
}

export class Stuff implements AnyObject{
  public id:string;
  public name:string;
  public description?:string;
  public creationDate?:string;
  public model?:any;
  public collection?:string;
}