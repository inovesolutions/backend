import * as express from 'express';
import {Request, Response, NextFunction} from 'express';
import { AccountController } from './account/Account.controller';
import { Database } from './database/Database';
import * as bodyParser from 'body-parser';
import * as Jwt from 'jsonwebtoken';
import { ProjectController } from './project/Project.controller';
import { StagingController } from './staging/Staging.controller';
import { ScenarioController } from './scenario/Scenario.controller';
import { PlotController } from './plot/Plot.controller';
import { ActorController } from './actor/Actor.controller';
import { StuffController } from './stuff/Stuff.controller';
import * as cors from 'cors';
import { UserController } from './user/User.controller';
import Axios from 'axios';
import { Config } from './Config';

class App {

  public express;

  constructor () {
    this.express = express();
    this.mountRoutes();
    Database.connect();
  }

  private mountRoutes (): void {

    const router = express.Router();

    router.get('/', (request:express.Request, response:express.Response) => {
      response.send(`ok`);
    })

    router.put('/account', AccountController.signIn);
    router.post('/account', AccountController.register);
    router.get('/api/account/profile', AccountController.getProfile);

    router.post('/api/project', ProjectController.create);
    router.put('/api/project/:id', ProjectController.update);
    router.get('/api/projects', ProjectController.getAll);
    router.get('/api/project/:id', ProjectController.get);

    router.post('/api/project/:projectId/scenario', ScenarioController.create);
    router.put('/api/project/:projectId/scenario/:id', ScenarioController.update);
    router.get('/api/project/:projectId/scenarios', ScenarioController.getAll);
    router.get('/api/project/:projectId/scenario/:id', ScenarioController.get);

    router.post('/api/project/:projectId/scenario/:scenarioId/plot', PlotController.create);
    router.put('/api/project/:projectId/scenario/:scenarioId/plot/:id', PlotController.update);
    router.get('/api/project/:projectId/scenario/:scenarioId/plots', PlotController.getAll);
    router.get('/api/project/:projectId/scenario/:scenarioId/plot/:id', PlotController.get);

    router.post('/api/project/:projectId/actor', ActorController.create);
    router.put('/api/project/:projectId/actor/:id', ActorController.update);
    router.get('/api/project/:projectId/actors', ActorController.getAll);
    router.get('/api/project/:projectId/actor/:id', ActorController.get);

    router.post('/api/project/:projectId/stuff', StuffController.create);
    router.put('/api/project/:projectId/stuff/:id', StuffController.update);
    router.get('/api/project/:projectId/stuffs', StuffController.getAll);
    router.get('/api/project/:projectId/stuff/:id', StuffController.get);

    router.post('/account/:account/project/:projectId/user', UserController.create)
    router.put('/account/:account/project/:projectId/user', UserController.signIn)
    router.post('/account/:account/project/:projectId/user/token', UserController.requestToken)

    router.post('/staging/:environment/:projectId/:scenarioId/:plotId', StagingController.run);
    
    this.express.use(cors());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(bodyParser.json());

    this.express.use('/staging/:environment/:projectId', 
    (request:Request, response:Response, next:NextFunction) => {
      let token = request.body.token || request.query.token || request.headers['authorization'];
      if(token){
        Axios.create({
          baseURL: Config.authServer
        }).get(`/${request.params.projectId}/tokens/${token}`)
        .then(() => next())
        .catch((error) => response.status(401).json({reason: error.message}))
      } else response.status(401).json({reason: 'Unauthorized'});
    })

    this.express.use('/account/:account/project/:projectId/user/token', 
    (request:Request, response:Response, next:NextFunction) => {
      let token = request.body.token || request.query.token || request.headers['authorization'];
      if(token){
        Jwt.verify(token, request.app.get('secretKey'), (error, decoded) => {
          if(error) return response.status(401).json({reason: 'Unauthorized'});
          request.app.set('userId', decoded);
          next();
        })
      } else response.status(401).json({reason: 'Unauthorized'});
    })

    this.express.use('/api', (request:Request, response:Response, next:NextFunction) => {
      let token = request.body.token || request.query.token || request.headers['authorization'];
      if(token){
        Jwt.verify(token, request.app.get('secretKey'), (error, decoded) => {
          if(error) return response.status(401).json({reason: 'Unauthorized'});
          request.app.set('account', decoded);
          next();
        })
      } else response.status(401).json({reason: 'Unauthorized'});
    });

    // this.express.use(mung.json((body, request, response)=> {
    //   return msgpack.encode(body, {
    //     codec: msgpack.codec.preset
    //   })
    // }));

    this.express.use('/', router);
  }
}

export default new App().express;
