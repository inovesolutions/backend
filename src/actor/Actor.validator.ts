import * as Revalidator from 'revalidator';
import { Actor } from './Actor.controller';

export class ActorValidator {
  private actorSchema:Revalidator.JSONSchema<any> = {
    properties: {
      id: {
        type: 'string',
        required: true
      },
      role: {
        type: 'string',
        required: true
      }
    }
  }

  public validate(actor:Actor):Revalidator.IReturnMessage{
    return Revalidator.validate(actor, this.actorSchema);
  }
}