import { AnyObject } from "../common/AnyObject.interface";
import { Request, Response } from "express";
import * as ShortId from 'shortid';
import { ActorValidator } from "./Actor.validator";
import { Database } from "../database/Database";

export class ActorController {
  public static create(request:Request, response:Response){
    const projectId = request.params.projectId;
    const actor:Actor = request.body;
    actor.id = ShortId.generate().replace('-', '_');
    actor.creationDate = new Date().toISOString();
    const validation = new ActorValidator().validate(actor);
    if(!validation.valid) return response.status(400).json(validation);
    Database.insert(projectId, 'actors', actor)
    .then(value => response.status(200).json(actor))
    .catch(reason => response.status(400).json(reason))
  }

  public static update(request:Request, response:Response){
    const projectId = request.params.projectId;
    const actorId = request.params.id;
    const actor:Actor = request.body;
    const validation = new ActorValidator().validate(actor);
    if(!validation.valid) return response.status(400).json(validation);
    Database.update(projectId, 'actors', {id: actorId}, actor)
    .then(value => response.status(200).json(actor))
    .catch(reason => response.status(400).json(reason))
  }

  public static getAll(request:Request, response:Response) {
    const projectId = request.params.projectId;
    const actors = Database.getTable(projectId, 'actors');
    Database.runOperation(actors)
    .then(result => result.toArray().then(value => response.status(200).json(value)))
    .catch(reason => response.status(204).send())
  }

  public static get(request:Request, response:Response){
    const projectId = request.params.projectId;
    const actorId = request.params.id;
    const actors = Database.getTable(projectId, 'actors');
    Database.runOperation(actors.get(actorId))
    .then(value => response.status(200).json(value))
    .catch(reason => response.status(400).json(reason))
  }
}

export class Actor implements AnyObject{
  public id:string;
  public role:string;
  public description?:string;
  public creationDate?:string;
}