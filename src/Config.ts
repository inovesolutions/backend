import * as RethinkDB from 'rethinkdb';

export class Config {

  public secret = 'fGJwB6wPMT';
  public connectionOptions:RethinkDB.ConnectionOptions = {};
  public static readonly authServer = 'http://localhost:4000';

  constructor() {
    this.connectionOptions.host = 'localhost';
    this.connectionOptions.port = 28045;
    // this.connectionOptions.user = 'admin';
    // this.connectionOptions.password = 'console';
  }
}