import { Request, Response } from "express";
import { Database } from "../database/Database";
import { Plot, Validation } from "../plot/Plot.controller";
import * as RethinkDB from 'rethinkdb';
import { Query } from "../database/Query";

export class StagingController{

  public static run(request:Request, response:Response){

    const projectId = request.params.projectId;
    const environment = request.params.environment;
    const scenarioId = request.params.scenarioId;
    const plotId = request.params.plotId;
    const plots = Database.getTable(projectId, 'plots');
    const db = `staging_${projectId}_${environment}`;

    Database.runOperation(plots.filter({id: plotId, scenarioId: scenarioId}))
    .then((cursor:RethinkDB.Cursor) => cursor.next<Plot>((error, plot) => {
        if(error) return response.status(400).send(error);
        switch (plot.action) {
          case Plot.Actions.VALIDATION:
            let result = Validation.validate(plot.payload as Validation, request.body);
            if(result.valid) response.status(200).json(result);
            else response.status(400).json(result);
            break;
          case Plot.Actions.INSERT:
            if(!plot.stuff) return response.status(400)
              .json({ reason: `Stuff not defined to ${plot.payload.type} operation`})
            let validation = Validation.validate({ schema: plot.stuff.model } as Validation, request.body);
            if (!validation.valid) return response.status(400).json({ reason: validation });
            Database.insert(db, plot.stuff.collection, request.body)
            .then(result => response.status(200).json(result))
            .catch(reason => response.status(400).json(reason))
            break;
          case Plot.Actions.UPDATE:
            if(!plot.stuff) return response.status(400)
              .json({ reason: `Stuff not defined to ${plot.payload.type} operation`})
            Database.update(db, plot.stuff.collection, request.body.query, request.body.update)
            .then(result => response.status(200).json(result))
            .catch(reason => response.status(400).json(reason))
            break;
          case Plot.Actions.QUERY:
            const dbName = `staging_${projectId}_${environment}`;
            let query = plot.payload as Query;
            let table = Database.getTable(dbName,  query.tableName);
            let sequence = Query.from(query, table);
            if (sequence) {
              Database.runOperation(sequence)
              .then(result => result.toArray().then(rows => response.status(200).json(rows)))
              .catch(reason => response.status(400).json(reason));
            }
            break;
          case Plot.Actions.INTEGRATION:
            response.status(200).send('OK');
            break;
          default:
            response.status(400).json({ reason: 'ACTION NOT SUPPORTED' });
            break;
        }
      }))
  }
}
