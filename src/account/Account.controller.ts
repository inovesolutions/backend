import { Request, Response } from "express";
import { Database } from "../database/Database";
import { Account } from "./Account.interface";
import { AccountValidator } from "./Account.validator";
import * as Crypto from 'crypto';
import * as Jwt from 'jsonwebtoken';
import * as ShortId from 'shortid';

export class AccountController {
  public static signIn (request:Request, response:Response) {
    
    let account:Account = request.body;
    let validation = new AccountValidator().validateSignin(account);
    if(!validation.valid) return response.status(400).json(validation.errors);

    let accountsTable = Database.getTable('scenario', 'accounts');
    Database.runOperation(accountsTable.filter({email: account.email}))
    .then(result => {
  
      result.next((error, row:Account) => {
        if(error) return response.status(401).json({reason: 'User not found'});
        let secretKey = request.app.get('secretKey');
        const decipher = Crypto.createDecipher('aes192', secretKey);
        let decrypted = decipher.update(row.password, 'hex', 'utf8');
        decrypted += decipher.final('utf8');

        if(account.password === decrypted){
          delete row.password;
          delete row.database;
          response.status(200).json(Jwt.sign(row, secretKey));
        } else {
          response.status(404).json({reason: 'Not Found'});
        }
      });
      
    });
  }

  public static register(request:Request, response:Response) {

    let account:Account = request.body;

    const validation = new AccountValidator().validateRegister(account);
    if(!validation.valid) return response.status(400).json(validation.errors);

    const secretKey = request.app.get('secretKey');
    const cipher = Crypto.createCipher('aes192', secretKey);
    let encrypted = cipher.update(account.password, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    account.password = encrypted;
    account.id = ShortId.generate().replace('-', '_');
    account.database = `${account.username}_${account.id}`;

    Database.insert('scenario', 'accountIdentities', {id: account.username})
    .then(result => {

      if(!result.inserted) return response.status(400).json(result);

      Database.insert('scenario', 'accounts', account)
      .then(result => {
        delete account.password;
        response.status(200).json(Jwt.sign(account, secretKey));
      })
      .catch(reason => response.status(400).json(reason))

    }).catch(reason => response.status(400).json(reason))
  }

  public static getProfile(request:Request, response:Response) {
    response.status(200).json(request.app.get('account'))
  }
}