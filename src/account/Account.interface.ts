export interface Account {
  id?:string;
  name:string;
  username:string;
  email:string;
  mobilePhone?:string;
  password:string;
  database:string;
}