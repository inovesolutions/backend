import { Account } from "./Account.interface";
import * as Revalidator from 'revalidator';

export class AccountValidator{


  private registerSchema:Revalidator.JSONSchema<Account> = {
    properties: {
      name: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      username: {
        type: 'string',
        allowEmpty: false,
        required: true,
        conform: (v) => /^[a-z0-9_]/.test(v)
      },
      email: {
        type: 'string',
        format: 'email',
        allowEmpty: false,
        required: true
      },
      password: {
        type: 'string',
        allowEmpty: false,
        required: true
      }
    }
  }

  private signinSchema:Revalidator.JSONSchema<any> = {
    properties: {
      email: {
        type: 'string',
        format: 'email',
        required: true
      },
      password: {
        type: 'string',
        required: true
      }
    }
  }

  public validateRegister(account:Account):Revalidator.IReturnMessage{
    return Revalidator.validate(account, this.registerSchema);
  }

  public validateSignin(account:Account):Revalidator.IReturnMessage{
    return Revalidator.validate(account, this.signinSchema);
  }

}