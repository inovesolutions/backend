import * as Revalidator from 'revalidator';
import { Scenario } from './Scenario.controller';

export class ScenarioValidator{

  private scenarioValidator:Revalidator.JSONSchema<Scenario> = {
    properties: {
      id: {
        type: 'string',
        required: true,
        pattern: /^\w+$/
      },
      name: {
        type: 'string',
        required: true
      }
    }
  };
  public validate(scenario:Scenario){
    return Revalidator.validate(scenario, this.scenarioValidator);
  }
}