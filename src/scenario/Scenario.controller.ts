import { Stuff } from "../stuff/Stuff.controller";
import { Actor } from "../actor/Actor.controller";
import { Plot } from "../plot/Plot.controller";
import { AnyObject } from "../common/AnyObject.interface";
import { Request, Response } from "express";
import { Database } from "../database/Database";
import { ScenarioValidator } from "./Scenario.validator";
import * as ShortId from 'shortid';
import * as moment from 'moment';

export class ScenarioController{

  public static create(request:Request, response:Response){

    const projectId = request.params.projectId;
    const scenario:Scenario = request.body;
    scenario.id = ShortId.generate().replace('-', '_');
    scenario.projectId = projectId;
    scenario.creationDate = moment().toISOString();
    const validation = new ScenarioValidator().validate(scenario);
    if(!validation.valid) return response.status(400).json(validation);
    Database.insert(projectId, 'scenarios', scenario)
    .then(() => {
      response.status(200).json(scenario);
    })
    .catch(reason => response.status(400).json(reason))

  }
  public static update(request:Request, response:Response){
    const projectId = request.params.projectId;
    const scenarioId = request.params.id;
    const scenario:Scenario = request.body;
    const validation = new ScenarioValidator().validate(scenario);
    if(!validation.valid) return response.status(400).json(validation);
    Database.update(projectId, 'scenarios', {id: scenarioId}, scenario)
    .then(value => response.status(200).json(scenario))
    .catch(reason => response.status(400).json(reason))
  }

  public static getAll(request:Request, response:Response) {
    const projectId = request.params.projectId;
    const scenarios = Database.getTable(projectId, 'scenarios');
    Database.runOperation(scenarios)
    .then(result => result.toArray().then(value => response.status(200).json(value)))
    .catch(reason => response.status(204).json([]))
  }

  public static get(request:Request, response:Response){
    const projectId = request.params.projectId;
    const scenarioId = request.params.id;
    const scenarios = Database.getTable(projectId, 'scenarios');
    Database.runOperation(scenarios.get(scenarioId))
    .then(value => response.status(200).json(value))
    .catch(reason => response.status(400).json(reason))
  }
}

export class Scenario implements AnyObject{
  public id:string;
  public projectId:string;
  public name:string;
  public actors?:Array<Actor>;
  public stuffs?:Array<Stuff>;
  public plots?:Array<Plot>;
  public description:string;
  public creationDate?:string;
}