import { Project } from './Project.controller';
import * as Revalidator from 'revalidator';

export class ProjectValidator{

  private projectSchema:Revalidator.JSONSchema<Project> = {
    properties: {
      id: {
        type: 'string',
        allowEmpty: false,
        required: true,
        conform: (v) => /^[a-zA-Z0-9_]*$/.test(v)
      },
      name: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      owner: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      creationDate: {
        type: 'string',
        allowEmpty: false,
        required: true
      }
    }
  }

  public validate(project:Project):Revalidator.IReturnMessage{
    return Revalidator.validate(project, this.projectSchema);
  }  
}