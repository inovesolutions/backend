import { Request, Response, NextFunction } from "express";
import { Database } from "../database/Database";
import { Scenario } from "../scenario/Scenario.controller";
import { ProjectValidator } from "./Project.validator";
import { Account } from "../account/Account.interface";
import * as ShortId from 'shortid';
import * as moment from 'moment';
import * as slug from 'slug';
export class ProjectController {

  public static create(request:Request, response:Response) {

    const project:Project = request.body;
    const account:Account = request.app.get('account');
    project.owner = account.username;
    project.creationDate = moment().toISOString();
    
    project.id = `${slug(project.name, '_')}_${ShortId.generate().replace('-', '_')}`;
    const validation = new ProjectValidator().validate(project);
    if (!validation.valid) return response.status(400).json({reason: validation});
      project.secretKey = ShortId.generate();
      project.domains = project.domains || [];
      if(!validation.valid) return response.status(400).json(validation);
    Database.insert(account.database, 'projects', project)
      .then(value => response.status(200).json(project))
      .catch(reason => response.status(400).json(reason))
  }

  public static update(request:Request, response:Response){
    const projectId = request.params.id;
    const account:Account = request.app.get('account');
    const project:Project = request.body;
    const validation = new ProjectValidator().validate(project);
    if(!validation.valid) return response.status(400).json(validation);
    Database.update(account.database, 'projects', {id: projectId}, project)
    .then(value => response.status(200).json(project))
    .catch(reason => response.status(400).json(reason))
  }

  public static getAll(request:Request, response:Response, next:NextFunction){
    let account:Account = request.app.get('account');
    let table = Database.getTable(account.database, 'projects');
    Database.runOperation(table.filter({owner: account.id}))
    .then(result => result.toArray().then(values => {
      response.status(200).json(values);
      next()
    })).catch(() => response.status(200).json([]))
  }

  public static get(request:Request, response:Response){
    let projectId:string = request.params.id;
    let account:Account = request.app.get('account');
    let table = Database.getTable(account.database, 'projects');
    Database.runOperation(table.get(projectId))
    .then(project => response.status(200).json(project))
    .catch(reason => response.status(400).json(reason))
  }
}

export class Project {
  public id:string;
  public name:string;
  public owner?:string;
  public creationDate?:string;
  public scenarios:Array<Scenario>;
  public description:string;
  public secretKey:string;
  public domains:Array<string> = [];
}