import * as Revalidator from 'revalidator';
import { User } from './User.controller';

export class UserValidator{


  private registerSchema:Revalidator.JSONSchema<User> = {
    properties: {
      name: {
        type: 'string',
        required: true
      },
      email: {
        type: 'string',
        format: 'email',
        required: true
      },
      username: {
        type: 'string',
        required: true
      },
      password: {
        type: 'string',
        required: true
      }
    }
  }

  private signinSchema:Revalidator.JSONSchema<any> = {
    properties: {
      username: {
        type: 'string',
        required: true
      },
      password: {
        type: 'string',
        required: true
      }
    }
  }

  public validateRegister(user:User):Revalidator.IReturnMessage{
    return Revalidator.validate(user, this.registerSchema);
  }

  public validateSignin(user:User):Revalidator.IReturnMessage{
    return Revalidator.validate(user, this.signinSchema);
  }

}