import { Actor } from "../actor/Actor.controller";
import {Request, Response} from 'express';
import { UserValidator } from "./User.validator";
import * as Crypto from 'crypto';
import * as Jwt from 'jsonwebtoken';
import * as ShortId from 'shortid';
import { Database } from "../database/Database";
import Axios from 'axios';
import { Config } from "../Config";
import { Project } from "../project/Project.controller";
export class UserController{

  public static signIn (request:Request, response:Response) {
    
    let user:User = request.body;
    let validation = new UserValidator().validateSignin(user);
    if(!validation.valid) return response.status(400).json(validation.errors);

    let accountsTable = Database.getTable(request.params.projectId, 'users');
    Database.runOperation(accountsTable.filter({username: user.username}))
    .then(result => {
      result.next((error, row:User) => {
        if(error) return response.status(400).json(error);
        let secretKey = request.app.get('secretKey');
        const decipher = Crypto.createDecipher('aes192', secretKey);
        let decrypted = decipher.update(row.password, 'hex', 'utf8');
        decrypted += decipher.final('utf8');

        if (user.password === decrypted) {
          response.status(200).json(Jwt.sign(row.id, secretKey));
        } else {
          response.status(404).json({reason: 'Not Found'});
        }
      });
      
    });
  }

  public static requestToken(request:Request, response:Response) {

    const clientId = request.params.projectId;
    const prefix = request.params.account;

    let projectsTable = Database.getTable(prefix, 'projects');
    Database.runOperation(projectsTable.filter({id: clientId}))
      .then(cursor => {
        cursor.next<Project>((error, project) => {
          if (error) return response.status(401).json({reason: 'Unauthorized'});
          let http = Axios.create({ baseURL: Config.authServer });
          http.post('/authorization', {
            responseType: 'code', prefix, clientId, state: request.app.get('userId')
          }).then((authResponse) => {
            http.post('/token', {
              grantType: 'authorization_code', clientId,
              code: authResponse.data.code, clientSecret: project.secretKey
            }).then(tokenResponse => response.status(200).json(tokenResponse.data))
            .catch(error => response.status(401).json({reason: 'Unauthorized'}))
          }).catch(error => response.status(400).json(error.message))
        })
      })
  }

  public static create(request:Request, response:Response) {

    let user:User = request.body;
    const validation = new UserValidator().validateRegister(user);
    if(!validation.valid) return response.status(400).json(validation.errors);

    const secretKey = request.app.get('secretKey');
    const cipher = Crypto.createCipher('aes192', secretKey);
    let encrypted = cipher.update(user.password, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    user.password = encrypted;
    user.id = ShortId.generate().replace('-', '_');

    Database.insert(request.params.projectId, 'userIdentities', {id: user.username})
    .then(result => {
      if(!result.inserted) return response.status(400).json(result);
      Database.insert(request.params.projectId, 'users', user)
      .then(() => {
        response.status(200).json(Jwt.sign(user.id, secretKey));
      })
      .catch(reason => response.status(400).json(reason))

    })
  }
}

export class User extends Actor{
  public name:string;
  public password:string;
  public username:string;
  public email:string;
  public profile?:any;
}